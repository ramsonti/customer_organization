package com.spacemind.custo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity security) throws Exception {
//		security.httpBasic().disable();
//		security.cors().and().csrf().disable();
		security.httpBasic().disable().cors().and().csrf().disable();
	}
	/*
	 * @Override protected void configure(HttpSecurity http) throws Exception { http
	 * .authorizeRequests() .antMatchers("/css/**").permitAll()
	 * .antMatchers("/webjars/**").permitAll() .antMatchers("/signup").permitAll()
	 * .antMatchers("/signup-process").permitAll()
	 * .antMatchers("/signup-success").permitAll() .anyRequest().authenticated()
	 * .and() .formLogin()
	 * .loginPage("/login").defaultSuccessUrl("/home").loginProcessingUrl(
	 * "/login-process").permitAll() .and()
	 * .logout().logoutSuccessUrl("/logout").permitAll();
	 * 
	 * }
	 */

	// ...
}