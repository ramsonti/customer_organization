package com.spacemind.custo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.spacemind.custo.employee.Employee;

public interface EmployeeRepository extends  JpaRepository<Employee,Integer> {
	@Query(value = "SELECT job_title FROM employees", nativeQuery = true)
	List<List<String>> findEmployeeData();
	@Query(value="select distinct job_title from employees",nativeQuery = true)
	List<String> uniqueEmployeeJobTitle();
	
	@Query(value="select distinct employee_number from employees where job_title=?1",nativeQuery = true)
	List<String> employeeNumber(String jobTitle);
	@Query(value="select customer_name from customers where sales_rep_employee_number in(select employee_number from employees where job_title=\"Sales Rep\")",nativeQuery = true)
	List<String> employeeList();
	@Query(value="select distinct first_name,employee_number from employees where job_title=?1",nativeQuery = true)
	List<List<String>> employeeName(String jobTitle);
	
}
