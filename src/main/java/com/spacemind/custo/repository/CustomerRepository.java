package com.spacemind.custo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.spacemind.custo.customer.Customer;

public interface CustomerRepository extends  JpaRepository<Customer,Integer> {
	@Query(value = "SELECT customer_number,customer_name FROM customers", nativeQuery = true)
	List<List<String>> findSomeData();
	@Query(value = "SELECT customer_name FROM customers", nativeQuery = true)
	List<String> fetchDataForAjax();
	
	@Query(value = "SELECT login_id,password FROM login_details", nativeQuery = true)
	List<List<String>> findUserData();
	@Query(value="SELECT email_id FROM login_details", nativeQuery = true)
	List<String> findUserEmails();
	
	@Query(value="Select customer_name from customers", nativeQuery = true)
	List<String >practice();
}

