package com.spacemind.custo.customer;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
@Entity
@Table(name = "customers")
@SecondaryTable(name = "login_details",  pkJoinColumns=@PrimaryKeyJoinColumn(name="loginId", referencedColumnName="customerNumber"))
public class Customer {
	@Id
	@Column(length=11)
	@Size(max=11,message="Customer number size can not be greater than 11")
	private String  customerNumber;
	
	@Size(min=2,max=50, message="Customer Name size(min=2,max=50)")
	@NotEmpty(message = "Customer Name cannot be empty")
	@Column( length = 50)
	private String customerName;
	
	@Email(message="Invalid Email address")
	@NotEmpty(message = "Email can not be empty")
	@Column(table="login_details",unique = true,length = 50)
	private String email_id;
	
	
	@Size(min=8,message="password size must be atleast 8")
	@NotEmpty(message="password can not be empty")
	@Column(table="login_details",length = 200, nullable = false)
	private String password;

	@Column(table="login_details",length = 1, nullable = false)
	private int user_type=1;
	
	@Column(table="login_details",length = 1, nullable = false)
	private int active_status=1;
	@Column(nullable=true)
	private String salesRepEmployeeNumber;
	
	
	@Size(min=2,max=50, message="Customer First Name size(min=2,max=50)")
	@NotEmpty(message = "Customer First Name cannot be empty")
	@Column(nullable = false, length = 50)
	private String contactFirstName;
	
	
	@Size(min=2,max=50, message="Customer Last Name size(min=2,max=50)")
	@NotEmpty(message = "Customer Last Name cannot be empty")
	@Column(nullable = false, length = 50)
	private String contactLastName;
	
	
	@Size(min=2,max=50, message="Phone size(min=2,max=50)")
	@NotEmpty(message = "Phone cannot be empty")
	@Column(nullable = false, length = 50)
	private String phone;
	
	
	@Size(min=2,max=50, message="Address size(min=2,max=50)")
	@NotEmpty(message = "Address cannot be empty")
	@Column(nullable = false, length = 50)
	private String addressLine1;
	
	@Size(max=50, message="Address size(max=50)")
	@Column(nullable = true, length = 50)
	private String addressLine2;
	
	
	@Size(min=2,max=50, message="City size(min=2,max=50)")
	@NotEmpty(message = "City cannot be empty")
	@Column(nullable = false, length = 50)
	private String city;
	
	
	@Size(min=2,max=50, message="State size(min=2,max=50)")
	@NotEmpty(message = "State cannot be empty")
	@Column(nullable = false, length = 50)
	private String state;
	
	
	@Column(nullable = true, length = 15)
	private String postalCode;
	
	
	@Size(min=2,max=50, message="Country size(min=2,max=50)")
	@NotEmpty(message = "Country cannot be empty")
	@Size(max=50)
	@Column(nullable = false)
	private String country;
	
	@Column(nullable = true)
	private double creditLimit=100000;
	
	public @Size(max = 11, message = "Customer number size can not be greater than 11") String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(@Size(max = 11, message = "Customer number size can not be greater than 11") String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}

	public void setSalesRepEmployeeNumber(String salesRepEmployeeNumber) {
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}

	public int getUser_type() {
		return user_type;
	}

	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}

	public int getActive_status() {
		return active_status;
	}

	public void setActive_status(int active_status) {
		this.active_status = active_status;
	}

}
