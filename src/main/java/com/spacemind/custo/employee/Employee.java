package com.spacemind.custo.employee;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Employees")
public class Employee {
@Id
private int employeeNumber;
@Column( length = 50,nullable=false)
private String firstName;
@Column( length = 50,nullable=false)
private String lastName;
@Column( length = 50,nullable=false)
private String email;
@Column( length = 10,nullable=false)
private String extension;
@Column( length = 10,nullable=false)
private String  officeCode;
@Column( length = 50,nullable=true)
private int reportsTo;
@Column( length = 50,nullable=false)
private String jobTitle;
public int getEmployeeNumber() {
	return employeeNumber;
}
public void setEmployeeNumber(int employeeNumber) {
	this.employeeNumber = employeeNumber;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getExtension() {
	return extension;
}
public void setExtension(String extension) {
	this.extension = extension;
}
public String getOfficeCode() {
	return officeCode;
}
public void setOfficeCode(String officeCode) {
	this.officeCode = officeCode;
}
public int getReportsTo() {
	return reportsTo;
}
public void setReportsTo(int reportsTo) {
	this.reportsTo = reportsTo;
}
public String getJobTitle() {
	return jobTitle;
}
public void setJobTitle(String jobTitle) {
	this.jobTitle = jobTitle;
}
@Override
public String toString() {
	return "Employee [employeeNumber=" + employeeNumber + ", firstName=" + firstName + ", lastName=" + lastName
			+ ", email=" + email + ", extension=" + extension + ", officeCode=" + officeCode + ", reportsTo="
			+ reportsTo + ", jobTitle=" + jobTitle + "]";
}


	
	
}
