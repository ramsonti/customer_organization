package com.spacemind.custo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.spacemind.custo.customer.Customer;
import com.spacemind.custo.repository.CustomerRepository;
import com.spacemind.custo.repository.EmployeeRepository;


import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@Controller
public class CustomerController {
	 private static final Logger logger = LogManager.getLogger(CustomerController.class);

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	
	int SHORT_ID_LENGTH=8;
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView viewHomePage(Model model) {
		 logger.debug("-------------------Some debug related code----------------");
		 ModelAndView modelAndView = new ModelAndView();
		Customer customer = new Customer();
		model.addAttribute("customer", customer);
		modelAndView.setViewName("index");
		return modelAndView;
	}
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@RequestMapping(value = "/login-process", method = RequestMethod.POST)
	public ModelAndView loginProcessing(@ModelAttribute Customer customer, Model model) {
		List<List<String>> userList = customerRepository.findUserData();
		boolean usernameFlag = false;
		boolean passwordFlag = false;
		for (List<String> oneUser : userList) {
			if (customer.getCustomerNumber().toString().equals(oneUser.get(0))) {
				usernameFlag = true;
				BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
				passwordFlag = encoder.matches(customer.getPassword(), oneUser.get(1));
				break;
			}
		}
		if (!usernameFlag) {
			model.addAttribute("usernameError", "Customer Id does not exist");
			model.addAttribute("usererrorClass", "no");
		}
		if (!passwordFlag && usernameFlag) {
			model.addAttribute("passwordError", "Invalid password");
			model.addAttribute("passworderrorClass", "no");
		}
		ModelAndView modelAndView = new ModelAndView();
		if (usernameFlag && passwordFlag) {
			model.addAttribute("id", customer.getCustomerNumber());
			modelAndView.setViewName("home");

		} else {
			modelAndView.setViewName("index");
		}
		return modelAndView;
	}
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@GetMapping("/login-success")
	public ModelAndView loginSuccess(Model model) {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName("/home");
		return modelAndView;
	}
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@GetMapping("/signup")
	public ModelAndView signUpPage(Model model) {
		Customer customer = new Customer();
		model.addAttribute("customer", customer);
		model.addAttribute("employeeTitle", employeeRepository.uniqueEmployeeJobTitle());
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("signup");
		return modelAndView;
	}
	@ResponseStatus(code=HttpStatus.CREATED)
	@RequestMapping(value = "/signup-process", method = RequestMethod.POST)
	public ModelAndView signUpProcess(@Valid Customer customer, Errors errors, Model model) {
		ModelAndView modelAndView = new ModelAndView();
		model.addAttribute("employeeTitle", employeeRepository.uniqueEmployeeJobTitle());

		List<ConstraintViolation<?>> violationsList = new ArrayList<>();
		if (errors.hasErrors()) {
			System.out.println(errors.getAllErrors());
			for (ObjectError e : errors.getAllErrors()) {
				violationsList.add(e.unwrap(ConstraintViolation.class));
			}
			for (ConstraintViolation<?> violation : violationsList) {
				model.addAttribute(violation.getPropertyPath() + "Error", violation.getMessage());
				model.addAttribute(violation.getPropertyPath() + "Css", "no");
			}
			modelAndView.setViewName("signup");
		} else {
			boolean matches = false;
			boolean successFlag = false;
			String passwordRegex = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,15}";
			matches = Pattern.matches(passwordRegex, customer.getPassword());
			if (!matches) {
				model.addAttribute("passwordError",
						"password must have atleast one lowercase letter, one uppercase letter, one numeric digit, and one special character and atleast 8 character eg. aaZZa44@,Geek@123");
				model.addAttribute("passwordCss", "no");
				successFlag = true;
			}
			List<String> emails = customerRepository.findUserEmails();
			for (String email : emails) {
				if (customer.getEmail_id().equalsIgnoreCase(email)) {
					model.addAttribute("emailError", "Already Exists choose unique email");
					model.addAttribute("emailCss", "no");
					successFlag = true;
				}
			}

			if (customer.getSalesRepEmployeeNumber().equalsIgnoreCase("select")) {
				model.addAttribute("salesRepEmployeeNumberError",
						"First Select an Employee Title from Title dropdown and then Select an Sales Rep Employee");
				model.addAttribute("salesRepEmployeeNumberCss", "no");
				successFlag = true;
			}
			if (successFlag) {
				modelAndView.setViewName("signup");
			} else {
				 String shortId = RandomStringUtils.randomAlphanumeric(SHORT_ID_LENGTH);
				 customer.setCustomerNumber(shortId);
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String encodedPassword = passwordEncoder.encode(customer.getPassword());
				customer.setPassword(encodedPassword);
				Customer newCustomer = customerRepository.save(customer);
				model.addAttribute("customerName", newCustomer.getCustomerName());
				model.addAttribute("customerNumber", newCustomer.getCustomerNumber());
				modelAndView.setViewName("signup_success");
			}
		}
		return modelAndView;
	}
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@GetMapping("/signup-success")
	public ModelAndView signupSuccess(Model model) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("signup_success");
		return modelAndView;
	}
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value = "/fetchSomeData", method = RequestMethod.GET)
	public List<String> ajaxjquery() {
		List<String> newList = customerRepository.fetchDataForAjax();
		return newList;
	}
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value = "/uniqueJobTitle", method = RequestMethod.GET)
	public List<String> uniqueTitle() {

		List<String> newList = employeeRepository.uniqueEmployeeJobTitle();

		return newList;
	}
	//back up
//	@ResponseStatus(code=HttpStatus.ACCEPTED)
//	@ResponseBody
//	@RequestMapping(value = "/uniqueList1/{name}", method = RequestMethod.GET)
//	public List<List<String>> uniqueResultData(@PathVariable("name") String name) {
//		List<List<String>> newList = employeeRepository.employeeName(name);
//		return newList;
//
//	}
	@ResponseStatus(code=HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value = "/uniqueList1", method = RequestMethod.POST)
	public List<List<String>> uniqueResultData(@RequestBody String str) {
		List<List<String>> newList=null;
		try {
			JSONObject jsonObject= new JSONObject( str);	
			newList = employeeRepository.employeeName(jsonObject.get("title").toString());
		}catch(Exception e) {
			logger.error(e.getMessage());
			
		}
		return newList;
	}

//	@ResponseBody
//	@RequestMapping(value="/practice",method=RequestMethod.POST)
////	@PostMapping("/practice")
//	public  ResponseEntity<?> fun(@RequestBody Customer customer ) {
//		 logger.debug("practice url called---------------"+customer.toString());
//		 System.out.println("--------------------------come into my controller---------------------------------");
//		List<String> list=customerRepository.practice();
//		 logger.info("practice url called---------------"+list);
//		return ResponseEntity.ok(list);		
////		return ResponseEntity.ok(HttpStatus.OK);
////		return customer;
////		return ResponseEntity.ok(customer);
//		 	
//	}

}
