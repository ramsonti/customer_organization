<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Created Successfully !!</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="/css/signup.css" rel="stylesheet">
</head>
<body>
	<div class="body-container">
		<h1 style="color: green">Customer created successfully</h1>
		<h3 style="color: blue">Customer Name : ${customerName }</h3>
		<h3 style="color: blue">Customer Id : ${customerNumber }</h3>
		<a class="btn btn-primary" type="button" href="/">login</a>
	</div>

</body>
</html>