<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome to home page</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="/css/signup.css" rel="stylesheet">
</head>
<body>
	<div class="body-container">
		<h1 style="color: green">Welcome to home Page</h1>
		<h3>Customer Id :${id }</h3>
		<br> <a type="button" href="/signup" class="btn btn-success">Signup</a>
		<br> <a type="button" href="/" class="btn btn-primary">Login</a>
		<br> <a type="button" href="/" class="btn btn-danger">Logout
		</a>
	</div>
</body>
</html>