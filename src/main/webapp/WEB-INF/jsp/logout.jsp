<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Logout Successfully!!!</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">

</head>
<body>
	<h1 style="color: red">Logout Successfully!!!</h1>
	<a type="button" href="/login" class="btn btn-primary">Login</a>
	<a type="button" href="/signup" class="btn btn-primary">Signup</a>
</body>
</html>