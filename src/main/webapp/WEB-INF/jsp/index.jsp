<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <%@page import="org.apache.log4j.Logger"%> --%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="org.apache.logging.log4j.LogManager"%>
 <%! static Logger logger = LogManager.getLogger("index_jsp"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="/css/signup.css" rel="stylesheet">
<link href="/css/indexPage.css" rel="stylesheet">

</head>
<body >
<%-- <% 
 logger.debug("Show DEBUG message");
           logger.info("Show INFO message");
           logger.warn("Show WARN message");
           logger.error("Show ERROR message");
   %> --%>
	<script>
		function validateForm() {
			const usernameId = document.getElementById("customerNumber");
			const passwordId = document.getElementById("password");
			if (!usernameId.value) {
				usernameId.focus();
				usernameId.style.borderColor = "red";
				passwordId.style.borderColor = "green";
				return false;
			}
			if (!passwordId.value) {
				passwordId.focus();
				usernameId.style.borderColor = "green";
				passwordId.style.borderColor = "red";
				return false;
			}
			usernameId.style.borderColor = "green";
			passwordId.style.borderColor = "green";
		}
	</script>
	<!--  <script>
	console.log($);
	 $.ajax({
		  type: "POST",
		  url: "/practice",
		  data:JSON.stringify({customer_name:"ram sonti"}),
		  dataType:"json",
		  contentType:"application/json",
		  success: function(data,status){
				console.log("something......comeinto response..."+data);
				alert("something");
			} 
		});
	
	</script> -->
	<div>
		<form:form action="/login-process" class="body-container"
			path="loginForm" method="post" modelAttribute="customer"
			onsubmit="return validateForm()">
			<h1 class="signup-header">Welcome to Customer Login Page</h1>
			<div class="form-field">
				<form:label for="customerNumber" path="customerNumber"
					class="form-label">Customer Id <span class="required">*</span></form:label>
				<form:input type="text" defaultValue="" required="true"
					path="customerNumber" name="customerNumber"
					class="form-control invalid" id="customerNumber"
					placeholder="Please Enter Customer Number" />

				<div id="isValidEmail" class="valid${usererrorClass}">
					${usernameError}</div>
			</div>
			<div class="form-field">
				<form:label for="password" path="password" class="form-label">Password <span class="required">*</span></form:label>
				<form:input type="password" required="true" path="password"
					name="password" class="form-control" id="password"
					placeholder="Please Enter your Password" />
				<div id="isValidPassword" class="valid${passworderrorClass}">
					${ passwordError}</div>
			</div>
			<br />
			<div class="form-field">
				<form:button name="loginButton" path="loginButton" id="loginButton"
					type="submit" class="btn btn-primary">Login</form:button>
			</div>

			<div class="form-field">
				<a type="button" href="/signup" class="btn btn-primary">Signup</a>
			</div>
		</form:form>

	</div>

</body>
</html>