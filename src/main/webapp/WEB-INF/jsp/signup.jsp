<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Signup Page</title>
<link href="/css/signup.css" rel="stylesheet">
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<script type="text/javascript">
		function validateForm() {
			return true;
		}
	</script>

	<form:form action="/signup-process" class="body-container"
		method="post" modelAttribute="customer"
		onsubmit="return validateForm()">
		<h1 class="signup-header">Welcome to Customer Sign up Page</h1>
		<div class="form-field">
			<form:label class="form-label" path="email_id">Email <span class="required">*</span></form:label>
			<form:input type="email" required="true" id="email"
				class="form-control" path="email_id"
				placeholder="Please Enter Your Email" />
			<div id="emailError" class="valid${emailCss}">${ emailError}</div>
		</div>

		<div class="form-field">
			<form:label for="password" path="password" class="form-label">Password <span class="required">*</span></form:label>
			<form:input type="password" required="true" class="form-control"
				id="password" path="password"
				placeholder="Please Enter Your Password" />
			<div id="passwordError" class="valid${passwordCss}">${ passwordError}</div>
		</div>

		<div class="form-field">
			<form:label for="customerName" path="customerName" class="form-label">Customer Name <span class="required">*</span></form:label>
			<form:input type="text" required="true" class="form-control"
				id="customerName" path="customerName"
				placeholder="Please Enter Customer Name" />
			<div id="customerNameError" class="valid${customerNameCss}">${ customerNameError}</div>
		</div>
		<div class="form-field">
			<form:label for="contactFirstName" path="contactFirstName"
				class="form-label">Contact First Name <span class="required">*</span></form:label>
			<form:input type="text" required="true" class="form-control"
				id="contactFirstName" path="contactFirstName"
				placeholder="Please Enter Contact First Name" />
			<div id="contactFirstNameError" class="valid${contactFirstNameCss}">${ contactFirstNameError}</div>
		</div>

		<div class="form-field">

			<label for="selectFieldMain" class="form-label">Employee Title <span class="required">*</span></label> <select
				class="form-select" id="selectFormFeildMain"
				aria-label="Default select example">
				<option>Select</option>

				<c:forEach items="${employeeTitle}" var="custo">
					<option value="${custo}">${custo}</option>
				</c:forEach>
			</select>
			<div id="selectFormFeildMainError" class="valid"></div>
		</div>
		<div class="form-field">

			<form:label for="salesRepEmployeeNumber"
				path="salesRepEmployeeNumber" class="form-label">Sales Rep Employee <span class="required">*</span></form:label>
			<form:select path="salesRepEmployeeNumber" class="form-select"
				id="selectFormSubFeild" aria-label="Default select example">
				<option value="select">select</option>
			</form:select>
			<div id="salesRepEmployeeNumberError"
				class="valid${salesRepEmployeeNumberCss}">${ salesRepEmployeeNumberError}</div>

		</div>
		<script type="text/javascript">
		
			$("#selectFormFeildMain")
					.on(
							"change",
							function(event) {

								
								$
										.ajax({
											url : "/uniqueList1",
											method : "Post",
											data:JSON.stringify({title:this.value}),
											dataType : 'json',
											contentType:"application/json",
											success : function(data,
													status) {
												console.log("data",data);
												$("#selectFormSubFeild").get(0).options.length = 0;
												$("#selectFormSubFeild").get(0).options[0] = new Option(
														"Select Employee",
														"select");

												$
														.each(
																data,
																function(index,
																		item) {

																	$(
																			"#selectFormSubFeild")
																			.get(
																					0).options[$(
																			"#selectFormSubFeild")
																			.get(
																					0).options.length] = new Option(
																			item[0],
																			item[1]);
																});
											}

										})
							})
		</script>
		<div class="form-field">
			<form:label for="contactLastName" path="contactLastName"
				class="form-label">Contact Last Name <span class="required">*</span></form:label>
			<form:input type="text" required="true" class="form-control" id="contactLastName"
				path="contactLastName" placeholder="Please Enter Contact Last Name" />
			<div id="contactLastNameError" class="valid${contactLastNameCss}">${ contactLastNameError}</div>
		</div>

		<div class="form-field">
			<form:label for="phone" path="phone" class="form-label">Phone <span class="required">*</span></form:label>
			<form:input type="text" required="true" class="form-control"
				id="phone" path="phone" placeholder="Please Enter Your Phone" />
			<div id="phoneError" class="valid${phoneCss}">${ phoneError}</div>
		</div>

		<div class="form-field">
			<form:label for="addressLine1" path="addressLine1" class="form-label">Address Line1 <span class="required">*</span></form:label>
			<form:input type="text" required="true" class="form-control"
				id="addressLine1" path="addressLine1"
				placeholder="Please Enter Address Line1 " />
			<div id="addressLine1Error" class="valid${addressLine1Css}">${ addressLine1Error}</div>
		</div>

		<div class="form-field">
			<form:label for="addressLine2" path="addressLine2" class="form-label">Address Line2</form:label>
			<form:input type="text"  class="form-control"
				id="addressLine2" path="addressLine2"
				placeholder="Please Enter Address Line2 " />
			<div id="addressLine2Error" class="valid${addressLine2Css}">${ addressLine2Error}</div>
		</div>

		<div class="form-field">
			<form:label for="city" path="city" class="form-label">City <span class="required">*</span></form:label>
			<form:input type="text" required="true" class="form-control"
				id="city" path="city" placeholder="Please Enter City " />
			<div id="cityError" class="valid${cityCss}">${ cityError}</div>
		</div>

		<div class="form-field">
			<form:label for="state" path="state" class="form-label">State <span class="required">*</span></form:label>
			<form:input type="text"  class="form-control"
				id="state" path="state" placeholder="Please Enter State " />
			<div id="stateError" class="valid${stateCss}">${ stateError}</div>
		</div>

		<div class="form-field">
			<form:label for="postalCode" path="postalCode" class="form-label">Postal Code</form:label>
			<form:input type="text"  class="form-control"
				id="postalCode" path="postalCode"
				placeholder="Please Enter Postal Code" />
			<div id="postalCodeError" class="valid${postalCodeCss}">${postalCodeError}</div>
		</div>

		<div class="form-field">
			<form:label for="country" path="country" class="form-label">country <span class="required">*</span></form:label>
			<form:input type="text" required="true" class="form-control"
				id="country" path="country" placeholder="Please Enter  Country" />
			<div id="countryError" class="valid${countryCss}">${countryError}</div>
		</div>

		<div class="form-field">
			<form:button type="submit" class="btn btn-primary">Signup</form:button>
		</div>
		<div class="form-field">
			<a type="button" href="/" class="btn btn-primary">Login</a>
		</div>
	</form:form>



</body>
</html>